
# Niveau 1 - Aufgabe 1.1
Als erstes habe ich die Dokumente (Filius-datei, Excel-datei) runtergeladen und in einen Ordner gepackt.
## Excel-Tabelle und Netzwerkaufteilung
___
Da es zwei Abteiungen gibt mit jeweils zwei Geräten, werde ich es in zwei gleich grosse Netzwerke aufteilen.

![Screenshot 2022-06-06 104023](/Bilder/Excel_Tabelle_01.PNG)
![Screenshot 2022-06-06 104023](/Bilder/Excel_Tabelle_02.PNG)

Die Netze wurden in einem /25 Netzwerk aufgeteilt

## Gerätkonfiguration und Testing
___
Die Filius-Vorlage steht bereits in der Aufgabe frei zur installation. 

![Screenshot 2022-06-06 104023](/Bilder/Netz.PNG)

Anschliessend habe ich gemäss der Netzdeklarierung der Excel-Datei die Geräte konfiguriert.

Die IP's und Subnetzmasken wurden wie folgt aufgeteilt:

### Verkauf
| Geräte   |      IP's      | 
|----------|:-------------:|
| Router Port 1 |  160.160.250.1 |
| PC-01 |  160.160.250.2   |
| PC-02 | 160.160.250.3 |

### Logistik

| Geräte   |      IP's      | 
|----------|:-------------:|
| Router Port 2 |  160.160.250.129 |
| PC-11 |  160.160.250.130   |
| PC-12 | 160.160.250.131 |


### Testing

Anschliessend habe ich versucht die Geräte im Netz als auch ausserhalb des Netzes die Geräte zu pingen. Aufgrund der richtigen Konfiguration war dies möglich.

![Screenshot 2022-06-06 104023](/Bilder/Testing_02.PNG)

PC-01 von der Abteilung Verkauf kann zu dem PC-12 von der Abteilung Logistik pingen

![Screenshot 2022-06-06 104023](/Bilder/Testing_03.PNG)