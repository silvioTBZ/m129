
# Niveau 1 - Aufgabe 1.2
Als erstes habe ich die Dokumente (Filius-datei, Excel-datei) runtergeladen und in einen Ordner gepackt.
## Excel-Tabelle und Netzwerkaufteilung
___
In diesem Fall gibt es 8 Abteilungen und alle haben die gleiche Anzahl an Geräten. Deswegen werde ich es in 8 gelich grossen Netzwerke aufteilen.

Weil es zu viele Netze sind, werde ich es in einem Kreisdiagramm visualisieren

![Screenshot 2022-06-06 104023](/Bilder/Kreisdiagramm.PNG)

Die Netze wurden im 25/ Subnetzaufgeteilt, um alle sauber gleichgross zu halten. Pro Netz werden 4 IP's benötigt. Eine IP je Gerät (in dem Fall 2 pro Netz) und die Netz- und Broadcastadresse.

![Screenshot 2022-06-06 104023](/Bilder/Niveau2_Aufgabe2_PCNetz.PNG)

## Gerätkonfiguration und Testing
___
Wie oben angedeutet habe ich die Netze und dementsprechend die IP's für die verschiedenene Abteilungen definiert.

Genaue IP verteilung wird später angezeigt.

Nach der Logik habe ich die ersten zwei freien IP's je Netz den Geräten vergeben. Die Ports an dem Router inklussive Subnetzmaske mussten ebefnalls angepasst werden.

Die IP's der Router-ports sind die gleichen wie die Gateways zu den jewilis verbundenen Netze/Abteilung. 




Wenn dazu überall die Subnetzmaske, IP und Gateway richtig verteilt ist, kann man in die anderen Netzwerke pingen

### Subnetz 1.

| Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 1 |  59.136.34.1 | 255.255.255.224|-
| PC-10 |  59.136.34.2   |255.255.255.224|59.136.34.1
| PC-20 | 59.136.34.3 |255.255.255.224   |59.136.34.1

### Subnetz 2.

| Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 2 |  59.136.34.33 | 255.255.255.224|-
| PC-40 |  59.136.34.34   |255.255.255.224|59.136.34.33
| PC-50 | 59.136.34.35 |255.255.255.224   |59.136.34.33

 ### Subnetz 3.
 
 | Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 3 |  59.136.34.65 | 255.255.255.224|-
| PC-70 |  59.136.34.66   |255.255.255.224|59.136.34.65
| PC-80 | 59.136.34.67 |255.255.255.224   |59.136.34.65

### Subnetz 4.

| Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 4 |  59.136.34.97 | 255.255.255.224|-
| PC-110 |  59.136.34.98  |255.255.255.224|59.136.34.97
| PC-120 | 59.136.34.99 |255.255.255.224   |59.136.34.97

### Subnetz 5.

| Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 5 |  59.136.34.129 | 255.255.255.224|-
| PC-130 |  59.136.34.130   |255.255.255.224|59.136.34.129
| PC-140 | 59.136.34.131 |255.255.255.224   |59.136.34.129

### Subnetz 6.

| Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 6 |  59.136.34.161 | 255.255.255.224|-
| PC-170 |  59.136.34.162   |255.255.255.224|59.136.34.161
| PC-180 | 59.136.34.163 |255.255.255.224   |59.136.34.161

### Subnetz 7.

| Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 7 |  59.136.34.193 | 255.255.255.224|-
| PC-200 |  59.136.34.194   |255.255.255.224|59.136.34.193
| PC-210 | 59.136.34.195 |255.255.255.224   |59.136.34.193

### Subnetz 8.

| Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 8 |  59.136.34.245 | 255.255.255.224|-
| PC-230 |  59.136.34.246   |255.255.255.224|59.136.34.245
| PC-240 | 59.136.34.247 |255.255.255.224   |59.136.34.245

### Testing

Anschliessend habe ich versucht die Geräte im Netz als auch ausserhalb des Netzes die Geräte zu pingen. Aufgrund der richtigen Konfiguration war dies möglich.

![Screenshot 2022-06-06 104023](/Bilder/Testing_1.2_01.PNG)

![Screenshot 2022-06-06 104023](/Bilder/Testing_1.2_02.PNG)