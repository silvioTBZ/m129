
# Niveau 1 - Aufgabe 2.1
Als erstes habe ich die Dokumente (Filius-datei, Excel-datei) runtergeladen und in einen Ordner gepackt.
# Excel-Tabelle und Netzwerkaufteilung
Da es vier Abteiungen gibt mit jeweils vier Geräten. Die nächsten zwei Netze sind in den letzten beiden Oktetten verteilt. Obwohl es in anderen Oktetten ist, sind die netze gleichgross.

![Screenshot 2022-06-06 104023](/Bilder/niveau2_1_exc1.PNG)
![Screenshot 2022-06-06 104023](/Bilder/niveau2_1_exc2.PNG)
![Screenshot 2022-06-06 104023](/Bilder/niveau2_1_exc3.PNG)
![Screenshot 2022-06-06 104023](/Bilder/niveau2_1_exc4.PNG)

Die Netze wurden in einem /25 Netzwerk aufgeteilt

# Gerätkonfiguration und Testing
Die Cisco-Vorlage steht bereits in der Aufgabe frei zur installation. 

![Screenshot 2022-06-06 104023](/Bilder/niveau2_1_netz.PNG)

Anschliessend habe ich gemäss der Netzdeklarierung der Excel-Datei die Geräte konfiguriert.

Die IP's und Subnetzmasken wurden wie folgt aufgeteilt:

### GL

| Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 1 |  133.95.48.1 | 255.255.255.128|-
| PC-8050 |  133.95.48.2   |255.255.255.128|133.95.48.1
| PC-8080 | 133.95.48.3 |255.255.255.128   |133.95.48.1

### Buchhaltung

| Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 2 |  133.95.48.129 | 255.255.255.128|-
| PC-8130 |  133.95.48.130   |255.255.255.128|133.95.48.129
| PC-8140 | 133.95.48.131 |255.255.255.128   |133.95.48.129

 ### Verkauf
 
 | Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 3 |  133.95.49.1 | 255.255.255.224|-
| PC-9050 |  133.95.49.2   |255.255.255.224|133.95.49.1
| PC-9080 | 133.95.49.3 |255.255.255.224   |133.95.49.1

### Einkauf

| Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 4 |  133.95.49.129 | 255.255.255.224|-
| PC-9130 |  133.95.49.130  |255.255.255.224|133.95.49.129
| PC-9140 | 133.95.49.131 |255.255.255.224   |133.95.49.129


## Testing

Anschliessend habe ich versucht die Geräte im Netz als auch ausserhalb des Netzes die Geräte zu pingen. Aufgrund der richtigen Konfiguration war dies möglich.

![Screenshot 2022-06-06 104023](/Bilder/niveau2_1_test.PNG)

Die Geräte können jetzt ins andere Netz undereinander pingen.

