
# Niveau 1 - Aufgabe 2.4
Als erstes habe ich die Dokumente (Filius-datei, Excel-datei) runtergeladen und in einen Ordner gepackt.
# Excel-Tabelle und Netzwerkaufteilung
Hier ist die Sachlafe eine andere. Hier gibt es vier Netze mit unterschiedlich vielen Geräten innerhalb. Dazu gehen die Geräte in diesem Netzwerk ins Internet raus durch den ISP-Router.

![Screenshot 2022-06-06 104023](/Bilder/niveaua2_4_netz1.PNG)
![Screenshot 2022-06-06 104023](/Bilder/niveaua2_4_netz2.PNG)

# Gerätkonfiguration und Testing
Die Cisco-Vorlage steht bereits in der Aufgabe frei zur installation. 

![Screenshot 2022-06-06 104023](/Bilder/niveau2_4_netz.PNG)

Anschliessend habe ich gemäss der Netzdeklarierung der Excel-Datei die Geräte konfiguriert.

Die IP's und Subnetzmasken wurden wie folgt aufgeteilt:

### Netz Betrieb

| Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 1 |  219.60.30.1 | 255.255.255.128|-
| PC-B002 |  219.60.30.2   |255.255.255.128|219.60.30.1
| PC-B003 | 219.60.30.3 |255.255.255.128   |219.60.30.1

### Netz Einkauf

| Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 2 |  219.60.30.129 | 255.255.255.128|-
| PC-E131 |  219.60.30.130   |255.255.255.128|219.60.30.129
| PC-E130 | 219.60.30.131 |255.255.255.128   |219.60.30.129

 ### Netz Verkauf
 
 | Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 3 |  133.95.49.1 | 255.255.255.224|-
| PC-V002 |  133.95.49.2   |255.255.255.224|219.60.31.1
| PC-V003 | 133.95.49.3 |255.255.255.224   |219.60.31.1

### Netz GL

| Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 4 |  219.60.31.33 | 255.255.255.240|-
| PC-9130 |  219.60.31.34  |255.255.255.240|219.60.31.33
| PC-9140 | 219.60.31.35 |255.255.255.240   |219.60.31.33


## Testing

Anschliessend habe ich versucht die Geräte im Netz als auch ausserhalb des Netzes die Geräte zu pingen. Man kann von dem Gerät direk den ISP Router pingen

![Screenshot 2022-06-06 104023](/Bilder/niveau2_4_test.PNG)

Die Geräte können jetzt ins andere Netz undereinander pingen.

