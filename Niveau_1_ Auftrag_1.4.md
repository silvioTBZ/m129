
# Niveau 1 - Aufgabe 1.4
Als erstes habe ich die Dokumente (Filius-datei, Excel-datei) runtergeladen und in einen Ordner gepackt.
# Excel-Tabelle und Netzwerkaufteilung

In diesem Fall gibt es 8 Abteilungen und alle haben die gleiche Anzahl an Geräten. Deswegen werde ich es in 8 gelich grossen Netzwerke aufteilen. Die Netze werden so aufgeteilt, dass nicht all zu viele Pl$tze für IP's offen bleiben


Die Netze wurden im 25/ Subnetzaufgeteilt, um alle sauber gleichgross zu halten. Pro Netz werden 4 IP's benötigt. Eine IP je Gerät (in dem Fall 2 pro Netz) und die Netz- und Broadcastadresse.

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4.PNG)

In diesemfall sind die Geräte bereits Konfiguriert, jedoch exestieren diverse Fehler was dafür sorgt, dass der Datenverkehr nicht sauber läuft.

# Fehlersuche und Korrektur

## Fehler 1
___

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_Fehler1.PNG)

Hier funktioniert der Ping nicht, da der Gateway ein falscher ist.
Hier ist das Ergebiss bei einem Ping-versuch:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_ping1.PNG)

## Korrektur 

Der Gateway ist falsch. Der Gateway sollte "170.11.83.9" sein. Wenn diese Änderung übernommen wurde, kann es wieder ausserhalb des Netzes pingen wie man hier sehen kann:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_Korr1.PNG)

## Fehler 2
___

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_Fehler2.PNG)

Bei dem zweiten Fehler ist es das gleiche Problem wie bei dem ersten. Der Gateway ist falsch und dabei kommt folgendes raus, wenn man versucht in das andere Netz zu pingen:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_ping2.PNG)

## Korrektur

Der Gateway ist falsch. Der Gateway sollte "170.11.83.9" sein. Wenn diese Änderung übernommen wurde, kann es wieder ausserhalb des Netzes pingen wie man hier sehen kann:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_korr2.PNG)

## Fehler 3
___

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_Fehler3.PNG)

Bei dem dritten Fehler ist immernoch der Gateway betroffen allerdings fehlt er in diesem Fall komplett. Deswegen funktioniert logischer weise der Ping von aussen wieder nicht wie man hier sehen kann:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_ping3.PNG)

## Korrektur

Wie schon erwähnt fehlt der Gateway. An dieser Stelle kommt folgende IP "170.11.83.41". Wie man hier sehen kann, funktioniert es wieder:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_korr3.PNG)

## Fehler 4
___

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_Fehler4.PNG)

Bei diesem Fehler ist die Subnetzmaske falsch, weshalb der ping auch nicht funktioniert wie man hier sieht:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_ping4.PNG)

## Korrektur

Da die Subnetzmaske falsch ist, muss die nur angepasst werden zur folgenden Subnetzmaske "255.255.255.248". Nach diese Änderung wird es wieder funktionieren wie man hier sieht:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_korr4.PNG)

## Fehler 5
___

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_Fehler5.PNG)

Hier ist die IP falsch. Bei dem letzten Oktet ist die Zahl zu hoch, weshalb der ping auch nicht funktioniert wie hier getestet:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_ping5.PNG)

## Korrektur

Hier muss lediglich nur die IP zudem jeweiligem Netz angepasst werden. Hier würde ich die IP "170.11.83.50" vergeben. Das funktioniert wie man hier sehen kann:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_korr5.PNG)

## Fehler 6
___

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_Fehler6.PNG)

Hierbei ist der Gateway wieder falsch. Dementsprechen funktioniert der Ping nicht ins andere Netzwerk wie man hier sieht:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_ping6.PNG)

## Korrektur

Für die Korrektur muss hier wieder der Gateway nur zu "170.11.83.57" geändert werden. Danach wird es auch wieder funktionieren wie man sieht:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_korr6.PNG)

## Fehler 7
___

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_Fehler7.PNG)

Hier ist die Subnetzmake wieder falsch, was dafür sorgt, dass der Ping nicht möglich ist wie man sieht:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_ping7.PNG)

## Korrektur

Bei diesem Fehler muss man nur die Subnetzmaske zu "255.255.255.248" umändern. Nach dieser Änderung funktioniert es auch wieder wie man hier sehen kann:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_korr7.PNG)

## Fehler 8
___

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_Fehler8.PNG)

Hier ist diesmal der die Subnetzmaske des 7 Ports vom Router betroffen. Bei dem Jeweiligen Netz funktioniert das pingen ausserhalb des Netzwerks auch nicht wie man hier sieht:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_ping8.PNG)

## Korrektur

Hierbei muss nur die Subnetzmaske an den CIDR angepasst werden zu "255.255.255.248". Danach funktioniert es wieder wie man sieht:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_Korr8.PNG)

## Fehler 9:
___

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_Fehler9.1.PNG)

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_Fehler9.2.PNG)

Hier ist der Fehler, dass zwei Geräte im gleichen Netz die gleiche IP haben. Deswegen scheitert auch der Ping wie man hier sieht:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_ping9.PNG)

## Korrektur

Die Lösung dafür ist für eines der beiden Geräten die nächste freie IP zu vergeben. Um es chronologisch sauber zu halte vergebe ich die IP "170.11.83.10" eines der Geräten. Danach geht es auch wieder wie man sieht:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_korr9.PNG)

## Fehler 10
___

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_Fehler10.PNG)

Die IP-Adresse von Port 4 des Routers ist falsch. Grundessen funktioniert der ping nicht wie man sieht:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_ping10.PNG)

## Korrektur

Hierbei muss die IP des Ports nur zu "170.11.83.25" geändert werden. Danach funktioniert es auch wieder wie man sieht:

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au4_korr10.PNG)