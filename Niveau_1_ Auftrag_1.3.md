
# Niveau 1 - Aufgabe 1.3
Als erstes habe ich die Dokumente (Filius-datei, Excel-datei) runtergeladen und in einen Ordner gepackt.
# Excel-Tabelle und Netzwerkaufteilung
In diesem Fall gibt es 4 Abteilungen und alle haben die gleiche Anzahl an Geräten. Deswegen werde ich es in 4 gelich grossen Netzwerke aufteilen.

Dafür nehmen wir das CIDR um nur soviele Plätze zu besetzen, sodass nicht allzuviel verschwendet wird.

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au3.PNG)

Die Netze wurden im 26/ Subnetzaufgeteilt, um alle sauber gleichgross zu halten. Pro Netz werden 4 IP's benötigt. Eine IP je Gerät (in dem Fall 2 pro Netz) und die Netz- und Broadcastadresse.


# Gerätkonfiguration und Testing
Wie oben angedeutet habe ich die Netze und dementsprechend die IP's für die verschiedenene Abteilungen definiert.

Genaue IP verteilung wird später angezeigt.

Nach der Logik habe ich die ersten zwei freien IP's je Netz den Geräten vergeben. Die Ports an dem Router inklussive Subnetzmaske mussten ebefnalls angepasst werden.

Die IP's der Router-ports sind die gleichen wie die Gateways zu den jewilis verbundenen Netze/Abteilung. 


Wenn dazu überall die Subnetzmaske, IP und Gateway richtig verteilt ist, kann man in die anderen Netzwerke pingen

### Subnetz 1.

| Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 1 |  25.30.120.1 | 255.255.255.192|-
| PC-2 |  25.30.120.2   |255.255.255.192|25.30.120.1
| PC-3 | 25.30.120.3 |255.255.255.192   |25.30.120.1

### Subnetz 2.

| Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 2 |  25.30.120.17 | 255.255.255.192|-
| PC-18 |  25.30.120.18   |255.255.255.192|25.30.120.17
| PC-19 | 25.30.120.19 |255.255.255.192  |25.30.120.17

 ### Subnetz 3.
 
 | Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 3 |  25.30.120.33 | 255.255.255.192|-
| PC-34 |  25.30.120.34   |255.255.255.192|25.30.120.33
| PC-35 | 25.30.120.35 |255.255.255.192   |25.30.120.33

### Subnetz 4.

| Geräte   |      IP's      | Subnetzmaske | Gateway |
|----------|:-------------:| :-------------: | ----------
| Router Port 4 |  25.30.120.49 | 255.255.255.192|-
| PC-80 |  25.30.120.50  |255.255.255.192|25.30.120.43
| PC-81 | 25.30.120.51 |255.255.255.192   |25.30.120.43


### Testing

Anschliessend habe ich versucht die Geräte im Netz als auch ausserhalb des Netzes die Geräte zu pingen. Aufgrund der richtigen Konfiguration war dies möglich.

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au3_testing1.PNG)

![Screenshot 2022-06-06 104023](/Bilder/niveau1_au3_testing2.PNG)